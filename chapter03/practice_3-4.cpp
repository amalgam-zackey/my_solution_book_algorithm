#include <iostream>
#include <vector>
#include <climits>

int main() {
    using namespace std;

    // 全体の要素数 N の入力
    int N;
    cin >> N;

    // 要素 a の入力
    vector<int> a(N);
    for (int i = 0; i < N ; i++) {
        cin >> a[i];
    }

    int max_value = INT_MIN;
    int min_value = INT_MAX;
    for (int i = 0; i < N; i++) {
        if (max_value < a[i]) {
            max_value = a[i];
        }
        if (a[i] < min_value) {
            min_value = a[i];
        }
    }

    int ans = max_value - min_value;
    cout << ans << endl;
}