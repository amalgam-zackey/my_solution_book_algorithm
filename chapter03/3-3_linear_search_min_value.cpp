#include <iostream>
#include <vector>

const int INF = 20000000;   // 十分大きな値に

int main() {
    using namespace std;

    // 全体の要素数Nの入力
    int N;
    cin >> N;

    // 要素aの入力
    vector<int> a(N);
    for (int i = 0; i < N ; i++) {
        cin >> a[i];
    }

    // 線形探索
    int min_value = INF;
    for (int i = 0; i < N; ++i) {
        if (a[i] < min_value) {
            min_value = a[i];
        }
    }

    // 結果出力 (-1 のときは見つからないケースを表す)
    cout << min_value << endl;
}

/*
root@57181a7a17cc:~/chapter03# ./3-3_linear_search_min_value
5
-100
10000
999
1
-1922     
-1922
*/