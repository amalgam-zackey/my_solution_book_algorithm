#include <iostream>
#include <vector>

const int INF = 20000000;       // 十分大きな値に

int main() {
    using namespace std;

    // 全体の要素数 N, 条件の値(その整数以上の範囲内) Kの入力を受け取る
    int N, K;
    cin >> N >> K;

    // N個の整数を2組の入力を受け取る
    vector<int> a(N), b(N);
    for (int i = 0; i < N; i++) {
        cin >> a[i];
    }
    for (int i = 0; i < N; i++) {
        cin >> b[i];
    }

    // 線形探索
    int min_value = INF;
    for (int i = 0; i < N; i++) {
        for (int j = 0; j < N; j++) {
            // 和が K 未満の場合は捨てる
            if (a[i] + b[j] < K) {
                continue;
            }

            // 最小値を更新
            if (a[i] + b[j] < min_value) {
                min_value = a[i] + b[j];
            }
        }
    }

    // 結果の出力
    cout << min_value << endl;
}

/*
root@57181a7a17cc:~/chapter03# ./3-4_linear_search_min_pair_sum
3
10
8
5
4
4
1
9
8
12
*/