#include <iostream>
#include <vector>
#include <climits>

int main() {
    using namespace std;

    // 全体の要素数 N の入力
    int N;
    cin >> N;

    // 要素 a の入力
    vector<int> a(N);
    for (int i = 0; i < N ; i++) {
        cin >> a[i];
    }

    unsigned int cnt_operation = 0;
    bool is_operating = true;
    while (is_operating) {
        bool are_all_even = true;
        for (int i = 0; i < N; i++) {
            if (a[i] % 2 != 0) {
                are_all_even = false;
            }
        }
        if (are_all_even) {
            for (int i = 0; i < N; i++) {
                a[i] = a[i] / 2;
            }
            cnt_operation++;
        } else {
            is_operating = false;
        }
    }

    cout << cnt_operation << endl;
}