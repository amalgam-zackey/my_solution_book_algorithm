#include <iostream>
#include <vector>
#include <climits>

int main() {
    using namespace std;

    // 全体の要素数 N の入力
    int N;
    cin >> N;

    // 要素 a の入力
    vector<int> a(N);
    for (int i = 0; i < N ; i++) {
        cin >> a[i];
    }

    // 僕の考えた解法
    int min_value = INT_MAX;
    int min_second_value = INT_MAX;
    // for (int i = 0; i < N; i++) {
    //     if (a[i] < min_value) {
    //         min_second_value = min_value;
    //         min_value = a[i];
    //     } else if (min_second_value == INT_MAX) {
    //         min_second_value = a[i];
    //     }
    // }

    // 本での解法
    for (int i = 0; i < N; i++) {
        if (a[i] < min_value) {
            min_second_value = min_value;
            min_value = a[i];
        } else if (a[i] < min_second_value) {
            min_second_value = a[i];
        }
    }

    cout << min_second_value << endl;
}