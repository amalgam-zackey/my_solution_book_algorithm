#include <iostream>
#include <vector>

int main() {
    using namespace std;

    // 全体の要素数N, 存在するか確認したい値vの入力
    int N, v;
    cin >> N >> v;

    // 要素aの入力
    vector<int> a(N);
    for (int i = 0; i < N ; i++) {
        cin >> a[i];
    }

    // 線形探索
    bool exist = false;     // 初期値はfalseに
    for (int i = 0; i < N; ++i) {
        if (a[i] == v) {
            exist = true;   // 見つかったら，フラグを立てる
        }
    }

    // 結果の出力
    if (exist) {
        cout << "Yes" << endl;
    } else {
        cout << "No" << endl;
    }
}

/*
root@57181a7a17cc:~/chapter03# ./3-1_linear_search
5 7
4
3
12
7
11
Yes
*/