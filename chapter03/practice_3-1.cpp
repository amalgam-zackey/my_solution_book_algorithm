#include <iostream>
#include <vector>

int main() {
    using namespace std;

    // 全体の要素数N, 存在するか確認したい値vの入力
    int N, v;
    cin >> N >> v;

    // 要素aの入力
    vector<int> a(N);
    for (int i = 0; i < N ; i++) {
        cin >> a[i];
    }

    // 線形探索
    int found_id = -1;      // 初期値は -1 などあり得ない値に
    for (int i = 0; i < N; ++i) {
        if (a[i] == v) {
            found_id = i;   // 見つかったら，添字を記録
        }
    }

    // 結果出力 (-1 のときは見つからないケースを表す)
    cout << found_id << endl;
}
