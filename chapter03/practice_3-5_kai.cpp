#include <iostream>
#include <vector>
#include <climits>

int main() {
    using namespace std;

    // 全体の要素数 N の入力
    int N;
    cin >> N;

    // 要素 a の入力
    vector<int> a(N);
    for (int i = 0; i < N ; i++) {
        cin >> a[i];
    }

    int min_cnt_divide = INT_MAX;
    // 各々の要素について，「2で何回割り切れるか？」=「2で割り切れる回数」を求める
    // その中で，「2で割り切れる回数」の最小値が答えになる
    for (int i = 0; i < N; i++) {
        int val = a[i];
        int cnt_divide = 0;
        while (val % 2 == 0) {
            cnt_divide++;
            val /= 2;
        }
        if (cnt_divide < min_cnt_divide) {
            min_cnt_divide = cnt_divide;
        }
    }

    cout << min_cnt_divide << endl;
}