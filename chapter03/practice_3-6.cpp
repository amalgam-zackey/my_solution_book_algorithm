#include <iostream>
#include <vector>
#include <climits>

int main() {
    using namespace std;

    // 上限値 K, 条件 N の入力
    int K, N;
    cin >> K >> N;

    int z;
    int cnt_pattern = 0;
    for (int x = 0; x <= K; x++) {
        for (int y = 0; y <= K; y++) {
            // N = x + y + zの式変形
            z = N - x - y;
            // zが 0 <= z <= Kを満たすかを判定
            // 満たしていれば，カウントアップ
            if (0 <= z && z <= K) {
                cnt_pattern++;
            }
        }
    }

    cout << cnt_pattern << endl;
}