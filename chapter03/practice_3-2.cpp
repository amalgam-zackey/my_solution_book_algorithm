#include <iostream>
#include <vector>

int main() {
    using namespace std;

    // 全体の要素数 N の入力
    int N;
    cin >> N;

    // 探索したい値 v の入力
    int v;
    cin >> v;

    // 要素 a の入力
    vector<int> a(N);
    for (int i = 0; i < N ; i++) {
        cin >> a[i];
    }

    // 何個含まれていたかをカウントする変数
    unsigned int cnt = 0;                   // 初期値は0
    for (int i = 0; i < N; i++) {
        if (a[i] == v) {
            cnt++;
        }
    }

    cout << cnt << endl;
}