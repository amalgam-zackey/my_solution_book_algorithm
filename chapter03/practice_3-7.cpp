#include <iostream>
#include <vector>

int main() {
    using namespace std;

    // 文字列 S の入力
    string S;
    cin >> S;
    int N = S.size();

    long long result = 0;
    // 「+」を挿入する位置を決めるfor文
    // S = "125" -> 1_2_5 (「_」に+が入る候補位置)
    // bit = 0b00 -> 1_2_5 -> どこにも+が入らず，125のまま
    // bit = 0b01 -> 1+2_5 -> 1 + 25
    // bit = 0b10 -> 1_2+5 -> 12 + 5
    // bit = 0b11 -> 1+2+5 -> 1 + 2 + 5
    for (int bit = 0; bit < (1 << (N - 1)); bit++) {
        // + と + との間の値を表す変数
        long long tmp = 0;
        for (int i = 0; i < N - 1; i++) {
            tmp *= 10;
            tmp += S[i] - '0';

            // + を挿入するときに，応えに tmp を加算．
            // そして，tmp を 0 に初期化．
            if (bit & (1 << i)) {
                result += tmp;
                tmp = 0;
            }
        }
        
        // 最後の + から，残りの部分を答えに加算
        tmp *= 10;
        tmp += S.back() - '0';
        result += tmp;
    }

    cout << result << endl;
}