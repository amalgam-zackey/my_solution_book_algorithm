#! /bin/bash

if [ $# -ne 1 ]; then
  echo "The number of specified arguments is $#." 1>&2
  echo "USAGE: ./run_docker_image.sh v1.0" 1>&2
  exit 1
fi

docker run -it -v /Users/zigzagzackey/workspace/my_solution_book_algorithm:/root -e DISPLAY="172.17.0.2:0" zigzagzackey/cpp_gcc_base:$1 
